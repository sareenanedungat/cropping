from PIL import Image
from django import forms
from django.core.files import File
from .models import Photo, Video
from io import BytesIO
import os
import subprocess




class PhotoForm(forms.ModelForm):
    x = forms.FloatField(widget=forms.HiddenInput())
    y = forms.FloatField(widget=forms.HiddenInput())
    width = forms.FloatField(widget=forms.HiddenInput())
    height = forms.FloatField(widget=forms.HiddenInput())

    class Meta:
        model = Photo
        fields = ('file', 'x', 'y', 'width', 'height', )

    def save(self):
        photo = super(PhotoForm, self).save()

        x = self.cleaned_data.get('x')
        y = self.cleaned_data.get('y')
        w = self.cleaned_data.get('width')
        h = self.cleaned_data.get('height')

        image = Image.open(photo.file)
        image.save(BytesIO(),format="jpeg" ,quality=60)
        print(image,"testt111111")
        cropped_image = image.crop((x, y, w+x, h+y))
        resized_image = cropped_image.resize((200, 200), Image.ANTIALIAS)
        resized_image.save(photo.file.path)
        print(resized_image,"testt2222")
        
        

        return photo


class VideoForm(forms.ModelForm):
    

    class Meta:
        model= Video
        fields= ["name", "videofile"]
    def save(self):
        video = super(VideoForm,self).save()
        name = self.cleaned_data.get('name')
        videofile = self.cleaned_data.get('videofile')
        
        
        return video