from django.apps import AppConfig


class CropPhotoConfig(AppConfig):
    name = 'crop_photo'
